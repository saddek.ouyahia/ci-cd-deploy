import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <p>
          Accéder au
        </p>
        <a
          className="App-link"
          href="https://gitlab.com/saddek.ouyahia/ci-cd-deploy"
          target="_blank"
          rel="noopener noreferrer"
        >
          Repo gitlab
        </a>
        <p>
          de Saddek OUYAHIA
        </p>
      </header>
    </div>
  );
}

export default App;
